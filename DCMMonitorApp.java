/*
 * Christopher Wilcox. 2010-2011. All rights reserved.
 * chris@crwilcox.com
 * 2011-1-13
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following condition(s)
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - 	Redistributions of this work must reproduce the above 
 *     	notice, this list of conditions and the following disclaimer in the
 *     	documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

public class DCMMonitorApp {  
	static Thread thread;  
	
	/**
	* main - 	This program is run to launch the DCMMonitor.  It is more or
	*			less a wrapper for the program
	*
	* @param args
	*/
	public static void main(String[] args) {  
		//String monitorDirectory = "/bin/jdicom/images/";
		String monitorDirectory = "/home/cwilcox/Desktop/images";
		String logFile = "/home/cwilcox/Desktop/DCMMonitorLog.log";
		Long pollingInterval = 2L;//In Seconds
		int numberOfSimultaneousImages = 5;//Number of images that will be opened at once
		
		DCMMonitor dcMon = new DCMMonitor(monitorDirectory, logFile, pollingInterval, numberOfSimultaneousImages);  

		thread = new Thread(dcMon);  
		thread.start();  
	}  
}  

