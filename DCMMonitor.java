/*
* Christopher Wilcox. 2010-2011. All rights reserved.
* chris@crwilcox.com
* 2011-1-13
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following condition(s)
* are met:
*
*   - Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*
*   - 	Redistributions of this work must reproduce the above 
*     	notice, this list of conditions and the following disclaimer in the
*     	documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
* IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
* THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
* LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import java.io.BufferedWriter;  
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.File;  
import java.io.FileWriter;  
import java.io.IOException;  
import java.util.Arrays;
import java.util.ArrayList;  
import java.util.Date;  
import java.util.List;  
public class DCMMonitor implements Runnable {  
	static int fileCount;  
	static Thread t;  
	static File[] existingFiles;  
	static boolean firstLoop = true;  
	int openGimps = 0;
	int maxSimultaneousImages;
	
	String monitorFolder;  //location of the folder being monitored
	String logfile; //logfile location
	long pollingInterval; // polling interval in seconds
	List list;   
	DCMMonitor(String monitorFolder, String logfile, long pollingInterval, int numberOfSimultaneousImages) {  
		this.monitorFolder = monitorFolder;  
		this.logfile = logfile;  
		this.pollingInterval = pollingInterval;
		this.maxSimultaneousImages = numberOfSimultaneousImages;
	}  
	/**
	* run
	*
	* This is the toplevel module.  It runs in a loop to 
	* continuously check for changes in the monitored directory
	* When a change is seen, it reports it to the log.
	*/
	public void run() {  
		t = DCMMonitorApp.thread;  
		Date date = new Date();  
		int curFileCount = 0;  
		File file = new File(this.monitorFolder);  
		File log = new File(this.logfile);  
		BufferedWriter writer = null;  
		FileWriter fstream = null;  

		File[] currentFiles = (File[]) null;  
		if (file.isDirectory()) {  
			curFileCount = file.listFiles().length;  
			if (firstLoop) {  
				existingFiles = file.listFiles();  
				fileCount = curFileCount;  
				firstLoop = false;  
			}  
		} else {  
			file.mkdir();  
		}  
		if (fileCount != curFileCount) {  
			try{
				//Setup the file writer, and get the current file list
				fstream = new FileWriter(log, true);  
				writer = new BufferedWriter(fstream);  
				currentFiles = file.listFiles(); 
				
				if (fileCount > curFileCount) {  
					printToLogTimestamp("FILES DELETED : " + (fileCount - curFileCount), writer);
					this.list = checkForDelete(existingFiles, currentFiles, writer);  
				} 
				else {  //fileCount < curFileCount 
					printToLogTimestamp("FILES ADDED : " + (curFileCount - fileCount), writer); 
					this.list = checkForAdd(existingFiles, currentFiles, writer);  
				}
				
				//after performing our checks, variables need to be updated, and the writer needs to be closed
				fileCount = curFileCount;  
				existingFiles = currentFiles;  
				writer.close();  
			} catch (IOException e) {  
				e.printStackTrace();  
			}  
		}
		try {  
			Thread.sleep(this.pollingInterval*1000);
		} catch (InterruptedException e) {  
			e.printStackTrace();  
		}  
		t.run();  
	} 
 
	
	/**
	* checkForAdd -	This method looks to see if there has been any added files, 
	*				and prints to log the files that have been added.
	* 
	* @param existing 	: Array of the files before
	* @param current	: Array of the files now
	* @return List		: Files found in current but not in existing
	*/
	private List checkForAdd(File[] existing, File[] current, BufferedWriter buffWriter) {  
		List<File> list = new ArrayList<File>();  
		for (int i = 0; i < current.length; ++i) {  
			//if we dont find current items in the existing, they must be new. Print these to log
			//also, if existing is of size 0, then we just print all of current to log as files
			if(!(Arrays.asList(existing).contains(current[i])) ||(existing.length ==0)){ 
				list.add(current[i]);
			}
		}
		if (list != null) {  
			for (int i = 0; i < list.size(); ++i) {  
				String filename = ((File) list.get(i)).getName();
				printToLogTimestamp(filename, buffWriter);
				//if(filename.indexOf(".dcm") != -1) {
					//This will need to change if you need to launch files of a different extension
					limitGimpInstances(maxSimultaneousImages);
					launchGimp(filename, monitorFolder);//Since it is an image, launch GIMP!
				//}								
			}  
		} 
		//}				
		return list;  
	}  
		
	
	/**
	* checkForDelete - 	This method looks to see if there has been a deletion, 
	*					and prints to log the files that have been deleted.
	* 
	* @param existing 	: Array of the files before
	* @param current	: Array of the files now
	* @return List		: Files found in existing but not in current
	*/
	private List checkForDelete(File[] existing, File[] current,  
	BufferedWriter writer) {  
		List<File> list = new ArrayList<File>();  
		for (int i = 0; i < existing.length; ++i) {  
			//if we dont find existing items in current, they must be deleted. Print these to log
			//also, if current is of size 0, then we just print all of existing to log as files that were deleted
			if(!(Arrays.asList(current).contains(existing[i])) ||(current.length ==0)){ 
				list.add(existing[i]);
			}
		}
		if (list != null) {  
			for (int i = 0; i < list.size(); ++i) {  
				printToLogTimestamp(((File) list.get(i)).getName(), writer);  
			}  
		}  
		return list;  
	} 
	
	
	/**
	* launchGimp - This method will launch gimp as a process
	* 
	* @param filename 			: The file to launch with GIMP
	* @param workingDirectory	: The directory to launch GIMP in
	*/
	private void launchGimp(String fileName, String workingDirectory){
		String cmd = "gimp " + fileName;
		File workDir =	new File(workingDirectory);

		try {
			Process p = Runtime.getRuntime().exec(cmd, null, workDir);
			openGimps++;
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}
	
	/**
	* limitGimpInstances - this stops gimp instances at a maxGimpInstances amount
	* 
	* @param maxGimpInstances 	: The file to launch with GIMP
	*/
	private void limitGimpInstances(int maxGimpInstances){
		String cmd = "killall gimp";
		
		if(openGimps >= maxGimpInstances){
			try {
				Process p = Runtime.getRuntime().exec(cmd);
				p.waitFor();
				openGimps = 0;
			}
			catch (Exception e) {
				System.out.println(e);
			}
		}
	}
	
	
	/**
	* printToLog - Prints a string as specified to the log on a line
	* 
	* @param stringToPrint 	: The string to print to the log
	* @param writer			: The buffer used to print to the log
	*/
	private void printToLog(String stringToPrint, BufferedWriter writer){
		try {  
			System.out.println(stringToPrint);  
			writer.newLine();  
			writer.write(stringToPrint);  
			writer.flush();  
		} catch (IOException e) {  
			e.printStackTrace();  
		}  
	}
	
	
	/**
	* printToLogTimestamp - Prints a string as specified to the log on a line with a Date on the front
	* 
	* @param str 		: The string to print to the log
	* @param writer		: The buffer used to print to the log
	*/
	private void printToLogTimestamp(String str, BufferedWriter writer){
		Date date = new Date();	
		String stringToPrint = date.toString() + " : " + str;
		printToLog(stringToPrint, writer);
	}
}  

