#!/bin/bash

echo "### Removing old .class files ###"
rm -f DCMMonitor.class DCMMonitorApp.class

echo "### Compiling DCMMonitorApp ###"
javac -Xlint:unchecked DCMMonitorApp.java

echo "### Starting DCMMonitorApp ###"
java DCMMonitorApp

